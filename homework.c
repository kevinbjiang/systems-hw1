/*
 * file:        homework.c
 * description: Skeleton for homework 1
 *
 * CS 5600, Computer Systems, Northeastern CCIS
 * Peter Desnoyers, Sep. 2012
 * $Id: homework.c 500 2012-01-15 16:15:23Z pjd $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#include "uprog.h"

/***********************************/
/* Declarations for code in misc.c */
/***********************************/

typedef int *stack_ptr_t;
extern void init_memory(void);
extern void do_switch(stack_ptr_t *location_for_old_sp, stack_ptr_t new_value);
extern stack_ptr_t setup_stack(int *stack, void *func);
extern int get_term_input(char *buf, size_t len);
extern void init_terms(void);

extern void  *proc1;
extern void  *proc1_stack;
extern void  *proc2;
extern void  *proc2_stack;
extern void **vector;


/***********************************************/
/********* Your code starts here ***************/
/***********************************************/

/*
 * Helper function for reading files into memory
 * returns 0 if success
*/
int readFileToMemory(char *file, void *address)
{
    FILE *pFile = fopen(file, "r");
    if (pFile == NULL)
    {
        return 1;
    }

    while (!feof(pFile))
    {
        size_t readSize = fread(address, 1, 256, pFile);
        address += readSize;
    }
    fclose(pFile);

    return 0;
}


/*
 * Question 1.
 *
 * The micro-program q1prog.c has already been written, and uses the
 * 'print' micro-system-call (index 0 in the vector table) to print
 * out "Hello world".
 *
 * You'll need to write the (very simple) print() function below, and
 * then put a pointer to it in vector[0].
 *
 * Then you read the micro-program 'q1prog' into memory starting at
 * address 'proc1', and execute it, printing "Hello world".
 *
 */
void print(char *line)
{
    /*  just wrap the std library's printf() */
    printf("%s", line);
}

void q1(void)
{
    /*
     * Your code goes here. Initialize the vector table, load the
     * code, and go.
     */

    /*  initialize vector table entry 0 to address of print function */
    vector[0] = &print;

    /*  load compiled microprogram to memory */
    if (readFileToMemory("q1prog", proc1) != 0)
    {
        print("Unable to find q1prog");
        return;
    }

    /*  run micro program */
    int (*microMain)(void) = proc1;
    microMain();
}


/*
 * Question 2.
 *
 * Add two more functions to the vector table:
 *   void readline(char *buf, int len) - read a line of input into 'buf'
 *   char *getarg(int i) - gets the i'th argument (see below)

 * Write a simple command line which prints a prompt and reads command
 * lines of the form 'cmd arg1 arg2 ...'. For each command line:
 *   - save arg1, arg2, ... in a location where they can be retrieved
 *     by 'getarg'
 *   - load and run the micro-program named by 'cmd'
 *   - if the command is "quit", then exit rather than running anything
 *
 * Note that this should be a general command line, allowing you to
 * execute arbitrary commands that you may not have written yet. You
 * are provided with a command that will work with this - 'q2prog',
 * which is a simple version of the 'grep' command.
 *
 * NOTE - your vector assignments have to mirror the ones in vector.s:
 *   0 = print
 *   1 = readline
 *   2 = getarg
 */

/*  globals to store the arguments */
char **argv = NULL;
int argc = 0;

void readline(char *buf, int len) /* vector index = 1 */
{
    /*  fgets takes care of the null termination and limiting the length to
        len - 1 */
    fgets(buf, len, stdin);
}

char *getarg(int i)		/* vector index = 2 */
{
    /*  bounds check */
    if (i < argc && i >= 0)
    {
        return argv[i];
    }

    return NULL;
}

/*  splits a line into words based on whitespace or newline delimeters */
void splitline(char *line, char ***tokens, int *count) 
{
    (*tokens) = NULL;
    (*count) = 0;
    char *token = NULL;
    while((token = strsep(&line, " \n\0"))) 
    {
        if(strlen(token)) 
        {
            (*tokens) = (char **)realloc((void *)(*tokens), sizeof(char *) * ((*count) + 1));
            (*tokens)[(*count)++] = token;
        }
    }
}

/*
 * Note - see c-programming.pdf for sample code to split a line into
 * separate tokens. 
 */
void q2(void)
{
    /*  init vector */
    vector[0] = &print;
    vector[1] = &readline;
    vector[2] = &getarg;

    /*  lineBuffer will store the actual line, argv pointers will point to
        characters within lineBuffer */
    char *lineBuffer = malloc(LINE_MAX);

    while (1)
    {
        /*  get a line */
        readline(lineBuffer, LINE_MAX);

        /*  split it into words */
        splitline(lineBuffer, &argv, &argc);

        /*  if zero words, continue */
        if(!argc)
        {
            continue;
        }

        /*  if first word is "quit", break */
        if(!strcmp(argv[0], "quit"))
        {
            break;
        }

        /*  load compiled microprogram to memory */
        if (readFileToMemory(argv[0], proc1))
        {
            printf("Unable to open program %s\n", argv[0]);
            continue;
        }

        /*  the program name is not an argument */
        argc--; /* the number of arguments minus the program name */
        argv++; /* move the arguments pointer up to the next word */
       
        /*  run micro program */
        int (*microMain)(void) = proc1;
        microMain();
    }
    /*
     * Note that you should allow the user to load an arbitrary command,
     * not just 'ugrep' and 'upcase', and print an error if you can't
     * find and load the command binary.
     */

    /*  cleanup */
    free(lineBuffer);
    free(argv);
}

/*
 * Question 3.
 *
 * Create two processes which switch back and forth.
 *
 * You will need to add another 3 functions to the table:
 *   void yield12(void) - save process 1, switch to process 2
 *   void yield21(void) - save process 2, switch to process 1
 *   void uexit(void) - return to original homework.c stack
 *
 * The code for this question will load 2 micro-programs, q3prog1 and
 * q3prog2, which are provided and merely consists of interleaved
 * calls to yield12() or yield21() and print(), finishing with uexit().
 *
 * Hints:
 * - Use setup_stack() to set up the stack for each process. It returns
 *   a stack pointer value which you can switch to.
 * - you need a global variable for each process to store its context
 *   (i.e. stack pointer)
 * - To start you use do_switch() to switch to the stack pointer for 
 *   process 1
 */

stack_ptr_t q3_stack = NULL;

void yield12(void)		/* vector index = 3 */
{
    do_switch((int **)&proc1_stack, proc2_stack);
}

void yield21(void)		/* vector index = 4 */
{
    do_switch((int **)&proc2_stack, proc1_stack);
}

void uexit(void)		/* vector index = 5 */
{
    /*  just need to give do_switch somewhere to write to but we'll never use
        this again */
    stack_ptr_t tmp = NULL;
    do_switch(&tmp, q3_stack);
}

void q3(void)
{
    /* init vector table */
    vector[0] = &print;
    vector[1] = &readline;
    vector[2] = &getarg;
    vector[3] = &yield12;
    vector[4] = &yield21;
    vector[5] = &uexit;

    /* load q3prog1 into process 1 and q3prog2 into process 2 */
    if (readFileToMemory("q3prog1", proc1) != 0)
    {
        printf("Unable to open q3prog1\n");
        return;
    }

    if (readFileToMemory("q3prog2", proc2) != 0)
    {
        printf("Unable to open q3prog2\n");
        return;
    }
    
    /* then switch to process 1 */
    proc1_stack = setup_stack(proc1_stack, proc1);
    proc2_stack = setup_stack(proc2_stack, proc2);
 
    do_switch(&q3_stack, proc1_stack); 
}


/***********************************************/
/*********** Your code ends here ***************/
/***********************************************/
